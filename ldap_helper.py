import ldap, ldif
from StringIO import StringIO
from ldap.cidict import cidict
from base64 import b64encode
from onboarding import app
from flask import flash

def bind(dn, passwd):
    con = ldap.initialize(app.config['LDAP_SERVER'])
    con.protocol_version = ldap.VERSION3	
    try:
        con.simple_bind_s(dn, passwd)
    except ldap.INVALID_CREDENTIALS:
        flash("Your username or password is incorrect.", 'error')
        return None
    except ldap.LDAPError, e:
        if type(e.message) == dict and e.message.has_key('desc'):
            flash(unpackError(e.message['desc']), 'error')
        else: 
            flash(unpackError(e), 'error')
        return None
    # ldap server returns a SimpleLDAPObject object after a successful bind
    if con.__class__.__name__ != "SimpleLDAPObject":
        flash('Unknown bind error', 'error')
        return None
    #print 'ok :%s' % dn
    return con

def unbind(con):
    if con.__class__.__name__ == "SimpleLDAPObject":
        con.unbind_s()

def escapeLDAP(ldap_string):
    if ldap_string == ldap.dn.escape_dn_chars(ldap_string):
        return ldap_string
    else:
        return None

def isValidDN(dn):
    if ldap.dn.is_dn(dn):
        return True
    return False

def unpackError(error):
    if type(error) is str or type(error) is unicode:
        return error
    
    result = "Error"
    if error.message:
        result = error.message['desc']
        if error.message.has_key('info'):
            return "%s, %s" % (result, error.message['info'])
    return result

def getParent(dn):
    i = dn.find(',')+1
    return dn[i:]

def get_rdn(dn):
    """Get the RDN string.
    get_red()->string rdn
    """
    pos1 = dn.find('=')+1
    pos2 = dn.find(',')
        
    return dn[pos1:pos2]

def get_search_results(results):
    """Given a set of results, return a list of LDAPSearchResult
    objects.
    """
    res = []

    if type(results) == tuple and len(results) == 2 :
        (code, arr) = results
    elif type(results) == list:
        arr = results

    if len(results) == 0:
        return res

    for item in arr:
        res.append( LDAPSearchResult(item) )

    return res


class LDAPSearchResult:
    """A class to model LDAP results.
    """

    dn = ''

    def __init__(self, entry_tuple):
        """Create a new LDAPSearchResult object."""
        (dn, attrs) = entry_tuple
        if dn:
            self.dn = dn
        else:
            return

        self.attrs = cidict(attrs)
        for key in self.attrs.keys():
            values = []
            for value in self.attrs[key]:
                if key == "jpegPhoto":
                    values.append(b64encode(value))
                    continue
                if key == "member":
                    values.append(value)
                    continue
                values.append(value.decode('utf-8'))
            self.attrs[key] = values

    def get_attributes(self):
        """Get a dictionary of all attributes.
        get_attributes()->{'name1':['value1','value2',...], 
				'name2: [value1...]}
        """
        return self.attrs

    def set_attributes(self, attr_dict):
        """Set the list of attributes for this record.

        The format of the dictionary should be string key, list of
        string alues. e.g. {'cn': ['M Butcher','Matt Butcher']}

        set_attributes(attr_dictionary)
        """

        self.attrs = cidict(attr_dict)

    def has_attribute(self, attr_name):
        """Returns true if there is an attribute by this name in the
        record.

        has_attribute(string attr_name)->boolean
        """
        return self.attrs.has_key( attr_name )

    def get_attr_values(self, key):
        """Get a list of attribute values.
        get_attr_values(string key)->['value1','value2']
        """
        return self.attrs[key]

    def get_attr_names(self):
        """Get a list of attribute names.
        get_attr_names()->['name1','name2',...]
        """
        return self.attrs.keys()

    def get_dn(self):
        """Get the DN string for the record.
        get_dn()->string dn
        """
        return self.dn

    def get_rdn(self):
        """Get the RDN string.
        get_red()->string rdn
        """
        return get_rdn(self.dn)

    def get_parent_dn(self):
        """Get the parent DN string"""
        i = self.dn.find(',')+1
        return self.dn[i:]

    def pretty_print(self):
        """Create a nice string representation of this object.

        pretty_print()->string
        """
        str = "DN: " + self.dn + "n"
        for a, v_list in self.attrs.iteritems():
            str = str + "Name: " + a + "n"
            for v in v_list:
                str = str + "  Value: " + v + "n"
        str = str + "========"
        return str

    def to_ldif(self):
        """Get an LDIF representation of this record.

        to_ldif()->string
        """
        out = StringIO()
        ldif_out = ldif.LDIFWriter(out)
        ldif_out.unparse(self.dn, self.attrs)
        return out.getvalue()
