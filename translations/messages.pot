# Translations template for PROJECT.
# Copyright (C) 2018 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2018-11-27 19:41+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: models.py:61
msgid "This element already exists."
msgstr ""

#: models.py:65 views.py:118
msgid "An account with this email already exists."
msgstr ""

#: models.py:77
msgid "Username is already taken"
msgstr ""

#: models.py:105 models.py:155 templates/ldapattribs.html:15
#: templates/managed_group.html:16
msgid "Name"
msgstr ""

#: models.py:109 templates/ldapattribs.html:22
msgid "Surname"
msgstr ""

#: models.py:113
msgid "Preferred language"
msgstr ""

#: models.py:116 templates/ldapattribs.html:84
msgid "Short biography"
msgstr ""

#: models.py:117
msgid "Tell us something about yourself"
msgstr ""

#: models.py:119
msgid "Your new username"
msgstr ""

#: models.py:124 models.py:149
msgid "Email address"
msgstr ""

#: models.py:128
msgid "Emails do not match"
msgstr ""

#: models.py:130
msgid "Repeat email address"
msgstr ""

#: models.py:131 templates/group.html:9 templates/ldapattribs.html:43
#: templates/managed_group.html:19 templates/profile.html:18
#: templates/remove_user.html:16
msgid "Collective"
msgstr ""

#: models.py:135
#, python-format
msgid ""
"I accept the <a href='%s' target='_blank'>conditions of "
"use</a>CONDITIONS_TERMS"
msgstr ""

#: models.py:141
msgid "New password"
msgstr ""

#: models.py:144
msgid "Passwords must match"
msgstr ""

#: models.py:146
msgid "Repeat password"
msgstr ""

#: models.py:158
msgid "Description"
msgstr ""

#: models.py:161
msgid "Website (optional)"
msgstr ""

#: models.py:164
msgid "Subject"
msgstr ""

#: models.py:167
msgid "Body"
msgstr ""

#: templates/profile_attributes.html:6 utils.py:19
msgid "My profile"
msgstr ""

#: utils.py:21
msgid "Registre"
msgstr ""

#: templates/support.html:5 utils.py:22
msgid "Support"
msgstr ""

#: utils.py:23
msgid "Contribute"
msgstr ""

#: utils.py:26
msgid "Users"
msgstr ""

#: templates/list_groups.html:10 utils.py:28
msgid "Services"
msgstr ""

#: templates/list_groups.html:25 utils.py:30
msgid "Collectives"
msgstr ""

#: utils.py:33
msgid "Logout"
msgstr ""

#: utils.py:35
msgid "Login"
msgstr ""

#: utils.py:468
msgid "Confirm your email at Commons Cloud"
msgstr ""

#: utils.py:470
msgid "_confirmEmail_text"
msgstr ""

#: utils.py:471
msgid "_confirmEmailAddress_email_text"
msgstr ""

#: utils.py:478
msgid "New user requesting an account"
msgstr ""

#: utils.py:480
msgid "_newUserWaiting_email_text"
msgstr ""

#: utils.py:501
msgid "Recover password at Commons Cloud"
msgstr ""

#: utils.py:503
msgid "_resetPassword_text"
msgstr ""

#: utils.py:508
msgid "Finish onboarding process at Commons Cloud"
msgstr ""

#: utils.py:510
msgid "_setPassword_text"
msgstr ""

#: views.py:47 views.py:708 views.py:1027 views.py:1357
msgid "DN not valid"
msgstr ""

#: views.py:320 views.py:456 views.py:945 views.py:1176
msgid "Updated OK"
msgstr ""

#: views.py:426 views.py:1181
msgid "Unable to update"
msgstr ""

#: views.py:471
#, python-format
msgid "Attribute '%s' not found"
msgstr ""

#: views.py:489 views.py:1021
msgid "User not found"
msgstr ""

#: views.py:494
msgid "User deleted"
msgstr ""

#: views.py:498
msgid "Cannot delete. User has been exported to LDAP"
msgstr ""

#: views.py:537
msgid "Email confirmed OK"
msgstr ""

#: views.py:561
msgid "Sent email to manager(s)"
msgstr ""

#: views.py:583
msgid "Password changed OK"
msgstr ""

#: views.py:625
msgid "We've sent you an email"
msgstr ""

#: views.py:675
msgid "Your password has been changed successfully"
msgstr ""

#: views.py:854 views.py:969 views.py:1157
#, python-format
msgid "Insufficient permission: %s"
msgstr ""

#: views.py:871
msgid "Cannot remove user from Primary collective"
msgstr ""

#: views.py:879
#, python-format
msgid "%s removed OK"
msgstr ""

#: views.py:1036
#, python-format
msgid "Added %s"
msgstr ""

#: views.py:1129
#, python-format
msgid "Failed to create %s"
msgstr ""

#: views.py:1228
msgid "Email sent OK"
msgstr ""

#: views.py:1253
msgid "Email sent"
msgstr ""

#: views.py:1256
#, python-format
msgid "User not found: %s"
msgstr ""

#: views.py:1273
msgid "No file part"
msgstr ""

#: views.py:1280
msgid "No selected file"
msgstr ""

#: views.py:1300
msgid "Avatar changed OK"
msgstr ""

#: views.py:1327
msgid "Credentials needed"
msgstr ""

#: views.py:1336 views.py:1349
msgid "Valid credentials needed"
msgstr ""

#: templates/404.html:7
msgid "404 Page not found"
msgstr ""

#: templates/adduser.html:18
msgid "Please edit"
msgstr ""

#: templates/adduser.html:22
msgid "Already exist in the database"
msgstr ""

#: templates/adduser.html:25
msgid "The new username will be"
msgstr ""

#: templates/adduser.html:36
msgid "Add user"
msgstr ""

#: templates/delete.html:7
msgid "Delete"
msgstr ""

#: templates/delete.html:8 templates/delete_user.html:13 templates/edit.html:30
#: templates/edit_group.html:42 templates/edit_group.html:44
#: templates/edit_photo.html:21 templates/managed_group.html:37
#: templates/modify_attribute.html:52 templates/modify_attribute.html:54
#: templates/remove_user.html:24 templates/send_group_email.html:43
msgid "Cancel"
msgstr ""

#: templates/delete_user.html:4
msgid "Delete entry"
msgstr ""

#: templates/delete_user.html:8
msgid "This can't be undone"
msgstr ""

#: templates/edit.html:22 templates/group.html:17
#: templates/modify_attribute.html:21
msgid "Edit"
msgstr ""

#: templates/edit.html:29 templates/edit_group.html:40
#: templates/modify_attribute.html:50
msgid "Save"
msgstr ""

#: templates/edit_group.html:8
msgid "Create group"
msgstr ""

#: templates/edit_group.html:9
msgid "Add group to "
msgstr ""

#: templates/edit_group.html:11
msgid "Edit group"
msgstr ""

#: templates/edit_photo.html:6
msgid "Edit avatar"
msgstr ""

#: templates/edit_photo.html:12
msgid "Image formats"
msgstr ""

#: templates/group.html:7 templates/remove_user.html:14
msgid "Service"
msgstr ""

#: templates/group.html:12
msgid "Group managers"
msgstr ""

#: templates/group.html:20
msgid "Managers"
msgstr ""

#: templates/group.html:42 templates/manage_groups.html:13
msgid "service_management_explained"
msgstr ""

#: templates/group.html:45
msgid "service_manager_group_explained"
msgstr ""

#: templates/group.html:48 templates/manage_groups.html:36
msgid "collective_management_explained"
msgstr ""

#: templates/group.html:51
msgid "collective_manager_group_explained"
msgstr ""

#: templates/group.html:60
msgid "Members"
msgstr ""

#: templates/group.html:68
msgid "Remove user from group"
msgstr ""

#: templates/group.html:77
msgid "Send email to members"
msgstr ""

#: templates/index.html:7 templates/onboarding-explained.html:35
msgid "Welcome"
msgstr ""

#: templates/index.html:9
msgid "index_page"
msgstr ""

#: templates/index.html:11
msgid "About us"
msgstr ""

#: templates/index.html:13
msgid "about_page"
msgstr ""

#: templates/ldapattribs.html:5
msgid "Edit profile"
msgstr ""

#: templates/ldapattribs.html:11 templates/managed_group.html:17
msgid "Username"
msgstr ""

#: templates/ldapattribs.html:17 templates/ldapattribs.html:24
#: templates/ldapattribs.html:34 templates/ldapattribs.html:45
#: templates/ldapattribs.html:60 templates/ldapattribs.html:67
#: templates/ldapattribs.html:75 templates/ldapattribs.html:86
msgid "edit"
msgstr ""

#: templates/ldapattribs.html:32 templates/managed_group.html:18
msgid "Email"
msgstr ""

#: templates/ldapattribs.html:39
msgid "Fullname"
msgstr ""

#: templates/ldapattribs.html:58
msgid "Avatar"
msgstr ""

#: templates/ldapattribs.html:65
msgid "Language"
msgstr ""

#: templates/ldapattribs.html:73
msgid "Quota"
msgstr ""

#: templates/ldapattribs.html:94 templates/profile.html:54
msgid "Your websites"
msgstr ""

#: templates/ldapattribs.html:96
msgid "Websites"
msgstr ""

#: templates/ldapattribs.html:106 templates/profile.html:67
msgid "Your groups"
msgstr ""

#: templates/ldapattribs.html:108
msgid "Groups"
msgstr ""

#: templates/list_groups.html:5
msgid "Groups of people"
msgstr ""

#: templates/list_groups.html:7
msgid "You can manage these groups"
msgstr ""

#: templates/list_groups.html:12 templates/list_groups.html:27
#: templates/manage_groups.html:7 templates/manage_groups.html:30
msgid "Add group"
msgstr ""

#: templates/list_groups.html:15 templates/manage_groups.html:15
msgid "Grant or deny people access to these services"
msgstr ""

#: templates/list_groups.html:30 templates/manage_groups.html:38
msgid "Add or remove people from these collectives"
msgstr ""

#: templates/login.html:5
msgid "Please login"
msgstr ""

#: templates/login.html:25 templates/registre.html:61
msgid "Forgotten your password?"
msgstr ""

#: templates/maintenance.html:5
msgid "Maintenance mode"
msgstr ""

#: templates/maintenance.html:7
msgid "maintenance_page"
msgstr ""

#: templates/manage_groups.html:5
msgid "Services you manage"
msgstr ""

#: templates/manage_groups.html:28
msgid "Collectives you manage"
msgstr ""

#: templates/manage_user.html:30
msgid "Onboarding not yet finished"
msgstr ""

#: templates/manage_user.html:31 templates/manage_user.html:44
msgid "Send the user an email to set password"
msgstr ""

#: templates/manage_user.html:34
msgid "Activate account"
msgstr ""

#: templates/manage_user.html:38
msgid "Onboarding complete"
msgstr ""

#: templates/manage_user.html:41
msgid "Waiting for user to set password"
msgstr ""

#: templates/manage_user.html:47
msgid "Send email again"
msgstr ""

#: templates/manage_users.html:54
msgid "Users you manage"
msgstr ""

#: templates/managed_group.html:36
msgid "Add member"
msgstr ""

#: templates/managed_group.html:39
msgid "Add new member"
msgstr ""

#: templates/managed_group.html:39
msgid "min. 4 chars"
msgstr ""

#: templates/managed_group.html:40
msgid "Search"
msgstr ""

#: templates/onboarding-explained.html:4
msgid "Hello"
msgstr ""

#: templates/onboarding-explained.html:6
msgid "Thank you for signing up at Commons Cloud"
msgstr ""

#: templates/onboarding-explained.html:10
msgid "We sent you an email to comfirm your email address"
msgstr ""

#: templates/onboarding-explained.html:14
msgid "(check spam)"
msgstr ""

#: templates/onboarding-explained.html:17
msgid "We create your account"
msgstr ""

#: templates/onboarding-explained.html:21
msgid "Please be patient. This might take one or two days."
msgstr ""

#: templates/onboarding-explained.html:24
msgid "You receive an e-mail to set your password"
msgstr ""

#: templates/onboarding-explained.html:26
msgid "Your account is activated"
msgstr ""

#: templates/onboarding-explained.html:37
msgid "Your username is"
msgstr ""

#: templates/onboarding-explained.html:39
msgid "Your password is"
msgstr ""

#: templates/onboarding-explained.html:50
msgid "You may login here"
msgstr ""

#: templates/onboarding-explained.html:58
msgid "You can manage your user profile here"
msgstr ""

#: templates/onboarding-explained.html:62
msgid "The whole process shouldn't take more than a day or two"
msgstr ""

#: templates/onboarding-explained.html:64
msgid "Kind regards"
msgstr ""

#: templates/profile.html:12
msgid "Your username"
msgstr ""

#: templates/profile.html:32
msgid "Edit your profile"
msgstr ""

#: templates/profile.html:33
msgid "Change password"
msgstr ""

#: templates/profile.html:42
msgid "Looking for help?"
msgstr ""

#: templates/profile.html:44
msgid "Visit our community support!"
msgstr ""

#: templates/recover_password.html:7 templates/recover_password.html:20
msgid "Recover password"
msgstr ""

#: templates/registre.html:22
msgid "Sign up"
msgstr ""

#: templates/registre.html:24
msgid "register_page"
msgstr ""

#: templates/registre.html:61
msgid "Already got an account and"
msgstr ""

#: templates/remove_user.html:11
msgid "User"
msgstr ""

#: templates/remove_user.html:23
msgid "Remove user"
msgstr ""

#: templates/send_group_email.html:7
msgid "Send email to"
msgstr ""

#: templates/send_group_email.html:9
msgid "service"
msgstr ""

#: templates/send_group_email.html:11
msgid "collective"
msgstr ""

#: templates/send_group_email.html:13
msgid "group of managers"
msgstr ""

#: templates/send_group_email.html:23
msgid "From"
msgstr ""

#: templates/send_group_email.html:25
msgid "Recipients"
msgstr ""

#: templates/send_group_email.html:25
msgid "Group members"
msgstr ""

#: templates/send_group_email.html:25
msgid "You will be sent a copy"
msgstr ""

#: templates/send_group_email.html:42
msgid "Send email"
msgstr ""

#: templates/set_password.html:6
msgid "Please set your new password"
msgstr ""

#: templates/set_password.html:14
msgid "Think of a longer, easier to remember passphrase"
msgstr ""

#: templates/set_password.html:19
msgid "is not a good password"
msgstr ""

#: templates/set_password.html:22
msgid "There were fleas last summer in Menorca."
msgstr ""

#: templates/set_password.html:23
msgid "is better"
msgstr ""

#: templates/set_password.html:31
msgid "Set password"
msgstr ""

#: templates/support.html:7
msgid "support_page"
msgstr ""

#: templates/userdata.html:7
msgid "User petition"
msgstr ""

#: templates/userdata.html:11
msgid "Delete petition"
msgstr ""

#: templates/userdata.html:59
msgid "Does not exist"
msgstr ""

#: templates/userdata.html:77
msgid "Notify manager"
msgstr ""

